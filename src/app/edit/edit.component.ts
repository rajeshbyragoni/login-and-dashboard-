import { Component, OnInit } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {first} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import  {CommonService} from '../service/common.service';

@Component({
	selector: 'app-edit',
	templateUrl: './edit.component.html',
	styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

	editDataForm : FormGroup;
	id : any;

	constructor(private fb : FormBuilder, private router : Router, private route : ActivatedRoute,  private _CommonService : CommonService ) {
		this.editCreateForm();


	}


	ngOnInit() {
		
		this.route.queryParams.subscribe(params => {
			
			this.id = params['id'];
			this.getEditData(params['id']);

		});
	}


	getEditData(id){
		this._CommonService.getIdData(id).subscribe(
			
			result =>{
				this.editDataForm.get('title').setValue(result.title);
				this.editDataForm.get('categories').setValue(result.categories);
				this.editDataForm.get('content').setValue(result.content);

			}, error => {

				console.log(error.toString());
			}

			
			);

	}



	editCreateForm(){
		this.editDataForm = this.fb.group({

			title: ['', Validators.required],
			categories: ['', Validators.required],
			content: ['', Validators.required],

		});

	}

	editClick(title, categories, content){
		this._CommonService.updateListData(title, categories, content, this.id).subscribe(

			result => {
				if(result.status){
					this.router.navigate(['/dashboard']);

				}
				//console.log(result);

			}, error => {

				console.log(error.toString());
			}


			);
	}



}
