import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders,  HttpParams} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable({
	providedIn: 'root'
})
export class CommonService {

	constructor(private http : HttpClient, private route: ActivatedRoute, private router: Router) { }



	// login page start here ...

	
	getLoginData(username, password):Observable<any> {

		const url = 'http://localhost/login/dashboard.php';
		const data = new FormData();
       
		return this.http.post<any>(url, data)
		.pipe(map((Response : Response) => {

			return Response;

		}),

		);
	}


	// login page end here ....

	



	// get method start here ....

	getListData():Observable<any> {

		const url ='http://reduxblog.herokuapp.com/api/posts';

		return this.http.get<any>(url)
		.pipe(map((Response : Response) => {
			return Response;

		}), catchError((error: Response) => {

			return throwError(error.status);

		}));
	}

	// get method end here ....



	// post method start here ....

	addListData(title, categories, content):Observable<any> {

		const url ='http://reduxblog.herokuapp.com/api/posts';

		const data = new FormData();
		data.append('title', title);
		data.append('categories', categories);
		data.append('content', content);

		return this.http.post<any>(url, data)
		.pipe(map((Response : Response) => {
			return Response;

		}), catchError((error: Response) => {

			return throwError(error.status);

		}));
	}

	// post method end here ....



	// update method start here ....


	/// get id/////

	getIdData(id):Observable<any> {

		const url ='http://reduxblog.herokuapp.com/api/posts/' + id;

		return this.http.get<any>(url)
		.pipe(map((Response : Response) => {
			return Response;

		}), catchError((error: Response) => {

			return throwError(error.status);

		}));
	}


	/// get id/////



	///post data...

	updateListData(title, categories, content, id):Observable<any> {

		const url ='http://reduxblog.herokuapp.com/api/posts/' + id;

		const data = new FormData();
		data.append('title', title);
		data.append('categories', categories);
		data.append('content', content);

		return this.http.post<any>(url, data)
		.pipe(map((Response : Response) => {
			return Response;

		}), catchError((error: Response) => {

			return throwError(error.status);

		}));
	}

	///post data...

	// update method end here ....





	// delete method start here ....

	deleteData(id):Observable<any> {

		const url ='http://reduxblog.herokuapp.com/api/posts/' + id;

		return this.http.delete<any>(url)
		.pipe(map((Response : Response) => {
			return Response;

		}), catchError((error: Response) => {

			return throwError(error.status);

		}));
	}


	// delete method end here ....

}
